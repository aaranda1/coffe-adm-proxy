const axios = require('axios')
const express = require('express')
const app = express();
app.use(express.json())
require('dotenv').config();

axios.defaults.baseURL = process.env.VUE_APP_BACKEND_HOST


async function getClientToken() {

    const body = {
        grant_type: "client_credentials",
        client_id: process.env.VUE_APP_CLIENT_ID,
        client_secret: process.env.VUE_APP_CLIENT_SECRET,
        scope: "*",
    }
    const response = await axios.post('/oauth/token', body)
        .catch((e) => {
            return e.response
        })

    return response.data.access_token

}

function getClient(headers) {

    return axios.create({
        headers: headers
    });

}

app.get('/api/*', async(req, resp) => {

    const client = getClient(req.headers)

    const response = await client.get(req.originalUrl)
        .catch((e) => {
            return e.response
        })

    resp.status(response.status).send(response.data)
})

app.post('api/*', async(req, resp) => {

    const client = getClient(req.headers)

    const response = await client.post(req.originalUrl, req.body)
        .catch((e) => {
            return e.response
        })

    resp.status(data.status).send(response.data.resource)
})

app.delete('api/*', async(req, resp) => {

    const client = getClient(req.headers)

    const data = await client.delete(req.originalUrl)
        .catch((e) => {
            return e.response
        })

    resp.status(data.status).send(data.data)
})

app.put('api/*', async(req, resp) => {

    const client = getClient(req.headers)

    const data = await axios.put(req.originalUrl, req.body)
        .catch((e) => {
            return e.response
        })

    resp.status(data.status).send(data.data)
})











app.post('/login', async(req, resp) => {

    const client = getClient(req.headers)

    const body = {

        "grant_type": "password",
        "client_id": process.env.VUE_APP_CLIENT_ID,
        "client_secret": process.env.VUE_APP_CLIENT_SECRET,
        "username": req.body.email,
        "password": req.body.password

    }

    const data = await client.post('/oauth/token', body)
        .catch((e) => {
            return e.response
        })

    resp.status(data.status).send(data.data)
})

app.post('/api/register/*', async(req, resp) => {

    const token = await getClientToken(req.headers)
    const client = getClient({ authorization: `Bearer ${token}` })
    const body = {
        grant_type: "client_credentials",
        client_id: process.env.VUE_APP_CLIENT_ID,
        client_secret: process.env.VUE_APP_CLIENT_SECRET
    }

    const response = await client.post(req.originalUrl, body)
        .catch((e) => {
            return e.response
        })
    console.log(response, req.headers)
    resp.status(response.status).send(response.data)
})




app.listen(process.env.SERVER_PORT, () => console.log(`server running in port ${process.env.SERVER_PORT}`));